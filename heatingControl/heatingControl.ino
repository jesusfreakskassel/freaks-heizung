#include <Stream.h>
#include <SoftwareSerial.h>
#include "constants.h"

int onModulePin = 2;
int ledPin = 13;
int relayPin = 12;

SoftwareSerial modemSerial(3,4);

void setup(){
    int8_t answer;
    pinMode(ledPin, OUTPUT);
    pinMode(relayPin, OUTPUT);
    digitalWrite(ledPin, LOW);
    pinMode(onModulePin, OUTPUT);
    Serial.begin(9600);
    modemSerial.begin(9600);

    Serial.println("Starting...");
    
    power_on();
    
    delay(3000);
    
    // sets the PIN code
    if(!sendATcommand("AT+CPIN?","+CPIN: READY",1000)) {
      char unlock_command[12];
      memset(unlock_command, '\0', 12);
      sprintf(unlock_command, "AT+CPIN=%d", PIN);
      sendATcommand(unlock_command, "OK", 2000);
    }
    
    delay(3000);
    
    Serial.println("Setting SMS mode...");
    sendATcommand("AT+CMGF=1", "OK", 1000);    // sets the SMS mode to text
    
    do {
      answer = sendATcommand("AT+CPMS=\"SM\",\"SM\",\"SM\"", "OK", 5000);     // selects the memory
    } while (answer != 1);
    
    delay(4000);
    
    remoteLog("WARNING: System (re-)booted");
    
}

void remoteLog(char* message) {
  return;
  sendATcommand("AT+CIPSHUT","OK",1000);
  sendATcommand("AT+CIPMUX=0","OK",1000);
  sendATcommand("AT+CSTT=\"pinternet.interkom.de\",\"\",\"\"","OK",1000);
  sendATcommand("AT+CGATT=1","OK",3000);
  sendATcommand("AT+CGATT?","+CGATT: 1", 1000);

  // bring up the GPRS connection
  sendATcommand("AT+CIICR","OK", 3000);

  // get my IP address
  sendATcommand("AT+CIFSR", "OK", FIVE_SECONDS);
  
  int answer = 0, tries = 0;
  
  while(answer == 0) { 
    answer = sendATcommand("AT+CIPSTART=\"UDP\",\"logs3.papertrailapp.com\",11833","CONNECT OK", TEN_SECONDS);
    tries++;
    if(tries > 3) {
      Serial.println("Could not establish remote connection");
      return; 
    }
  }
  
  sendATcommand("AT+CIPSEND",">",2000);
  
  char* message_prefix ="<22>arduino: ";
  
  char buffer2[400];
  sprintf(buffer2, "%s%s%c", message_prefix, message, '\032');
  
  answer = sendATcommand(buffer2, "OK", FIVE_SECONDS);

  Serial.print("cipsend buffer sent, code ");
  Serial.println(answer,DEC);
  sendATcommand("AT+CIPCLOSE=0", "CLOSE OK", TWO_SECONDS);
  sendATcommand("AT+CGATT=0","OK",3000);
}


void heatingOn(char* SMS) {
  Serial.println("Heating system on");
  char message[300];
  memcpy(message, '\0', 300);
  sprintf(message, "INFO: Heating system on, caused by %s", SMS);
  Serial.println(message);
  remoteLog(message);
  digitalWrite(ledPin, HIGH);
  digitalWrite(relayPin, HIGH);
}

void heatingOff(char* SMS) {
  char message[300];
  memcpy(message, '\0', 300);
  sprintf(message, "INFO: Heating system off, caused by %s", SMS);
  Serial.println(message);
  remoteLog(message);
  digitalWrite(ledPin, LOW);
  digitalWrite(relayPin, LOW);
}

void loop(){

  Serial.print("The time is ");
  Serial.println(millis());
  
  int8_t answer;
  uint8_t i = 0;
  char SMS[200];
  int next;
  uint32_t previous;
  uint32_t now;
  boolean timedOut = false;
  
  memset(SMS, '\0', 200);
  
  next = isSMSPresent();
  Serial.print("next: ");
  Serial.println(next);
  if(next < 0 || next > 10) {
    Serial.println("No SMS present or invalid");
    delay(5000);
    return;
  }
  delay(1000);
  
  char command[10];
  memset(command, '\0', 10);
  sprintf(command, "AT+CMGR=%d", next);
  
  Serial.print("++ SMS available at position ");
  Serial.println(next);
  
  answer = sendATcommand(command, "+CMGR:", TWO_SECONDS);    // reads the first SMS

  if (answer == 1)
  {
      answer = 0;

      previous = millis();

      // wait until we have something to read...
      while(modemSerial.available() == 0);
      Serial.print("Took ");
      Serial.print(millis() - previous);
      Serial.println("ms for SMS to be ready");

      previous = millis();
      
      // this loop reads the data of the SMS
      do{
          // if there are data in the UART input buffer, reads it and checks for the asnwer
          if(modemSerial.available() > 0) {    
              SMS[i] = modemSerial.read();
              i++;
              // check if the desired answer (OK) is in the response of the module
              if (strstr(SMS, "OK") != NULL)    
              {
                  answer = 1;
              }
          }
          now = millis();
          if((now - previous) > TEN_SECONDS) {
            timedOut = true;
            Serial.print("!! Reading SMS timed out after ");
            Serial.print(TEN_SECONDS);
            Serial.print(" ms: Started / ended: ");
            Serial.print(previous);
            Serial.print("/");
            Serial.print(now);
            Serial.print("=");
            Serial.println(now - previous);
          }
      } while(answer == 0 && !timedOut);    // Waits for the asnwer with time out

      Serial.print("Took ");
      Serial.print(millis() - previous);
      Serial.println("ms to read SMS");
      
      SMS[i] = '\0';
      
      if(strstr(SMS, "LARS") != NULL && strstr(SMS, "GASSI") != NULL) {
        Serial.println(SMS);
        heatingOn(SMS);
      } 
      else if (strstr(SMS, "LARS") != NULL && strstr(SMS, "SCHLAFEN") != NULL) {
        Serial.println(SMS);
        heatingOff(SMS);
      } 
      else {
        Serial.print("Unknown SMS received: ");
        char message[300];
        memcpy(message, '\0', 300);
        sprintf(message, "WARNING: unknown SMS received: %s", SMS);
        remoteLog(message);
        Serial.println(SMS);
      }

     memset(command, '\0', 10);
     sprintf(command, "AT+CMGD=%d", next);
     if(!sendATcommand(command, "OK", TWO_SECONDS)){
       Serial.println("ERROR: Could not delete message.");
     }
     
  } else {
    Serial.println("No SMS at position present");
  }
  
  Serial.println("Waiting for 5 seconds");
  delay(5000);
}

int isSMSPresent() {
  int8_t answer;
  char buffer[4];
  int i = 0;
  answer = sendATcommand("AT+CMGL=\"ALL\"", "+CMGL: ", TWO_SECONDS);
  if (answer == 1) {
    answer = 0;

    char nextCharArr[3];
    String next = readUntil(&modemSerial, ",", 4, answer, ONE_SECOND);
    next.toCharArray(nextCharArr, 3);
    Serial.print("Found SMS at ");
    Serial.println(next);
    clearSerialBuffer(&modemSerial);
    if(answer == 1) {
      return atoi(nextCharArr);
    }
  }
  Serial.println("<<< isSMSPresent: -1");
  return -1;
  
}

String readUntil(Stream* stream, char* expected, int bufsize, int8_t &success, unsigned int timeout) {
  char buff[bufsize];
  int i= 0;
  int8_t answer = 0;
  int len = 0;
  uint32_t previous;
  memset(buff, '\0', bufsize);

  previous = millis();
  
  // this loop reads the data from the stream
  do{
      // if there are data in the UART input buffer, reads it and checks for the asnwer
      if(stream->available() > 0) {    
          buff[i] = stream->read();
          i++;
          // check if the desired answer is in the response of the module
          char* match;
          if ((match = strstr(buff, expected)) != NULL && match-buff>0) {
            len = match - buff;
            answer = 1;
          } 
      }
  } while(answer == 0 && i < bufsize && (millis() - previous) < timeout);

  success = answer;
  
  return String(buff).substring(0, len);
}

/**
 * clears out the serial buffer
 * 
 * parameters:
 * *buffer Stream Stream that should be cleared
 */
void clearSerialBuffer(Stream *buffer) {
  while( buffer->available() > 0) buffer->read();
}

void power_on(){
  uint8_t answer = 0;
  // checks if the module is started
  answer = sendATcommand("AT", "OK", 2000);
  if (answer == 1) {
    // modem was started already, power cycle twice
    Serial.println("Modem on alreay, shutting down...");
    powerCycleModem();
  }
  Serial.println("Starting modem...");
  powerCycleModem();
  waitForModemReady();
}

void waitForModemReady() {
  uint8_t answer = 0;
  // waits for an answer from the module
  while(answer == 0){     // Send AT every two seconds and wait for the answer
    answer = sendATcommand("AT", "OK", 2000);    
  }
}

void powerCycleModem() {
  // power on pulse
  digitalWrite(onModulePin,HIGH);
  delay(3000);
  digitalWrite(onModulePin,LOW);
  delay(1000);
}

int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int timeout){
    uint8_t x=0,  answer=0;
    char response[100];
    unsigned long previous;

    memset(response, '\0', 100);    // Initialise the string

    delay(100);

    while( modemSerial.available() > 0) modemSerial.read();    // Clean the input buffer

    modemSerial.println(ATcommand);    // Send the AT command 
    Serial.print(">> ");
    Serial.println(ATcommand);

    x = 0;
    previous = millis();

    bool timedOut = false;
    // this loop waits for the answer
    do{
        // if there are data in the UART input buffer, reads it and checks for the asnwer
        if(modemSerial.available() != 0){    
            response[x] = modemSerial.read();
            x++;
            // check if the desired answer is in the response of the module
            if (strstr(response, expected_answer) != NULL)    
            {
                answer = 1;
                Serial.print("<< ");
                Serial.println(response);
            } 
        }
        if((millis() - previous) > timeout) {
          timedOut = true;
        }
        // Waits for the asnwer with time out
    }while((answer == 0) && timedOut == false);
    
    if(timedOut) {
      Serial.print("## Timeout waiting for ");
      Serial.print(expected_answer);
      Serial.print(", got");
      Serial.println(response);
    }
    
    return answer;
}
