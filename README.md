Schaltung für Gemeindeheizung via Relay

ACHTUNG: Mit Arduino 1.0.5 gibt es Probleme, Arduino 1.6.7 funktioniert einwandfrei.

Funktionsweise:

* SMS mit Kennung "LARS GASSI" schaltet die Heizung an.
* SMS mit Kennung "LARS SCHLAFEN" schaltet die Heizung ab.

Anschluss:

* Relay In auf Pin 12

Betrieb:

* Jumper Ard.USB auf Ard stellen

Neuprogrammieren:

* Um neue Software auf den Chip zu spielen, müssen die beiden Jumper Ard./USB gezogen werden.

Um direkt mit dem GSM-Chipsatz zu kommunizieren:

* Jumper Ard./USB auf USB stellen